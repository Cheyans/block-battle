#
from collections import deque

from Bot.Strategies.AbstractStrategy import AbstractStrategy
from .Heuristic import bestPositionHeuristic, bestPathHeuristic
from .Element import Element
import numpy as np


class SearchStrategy(AbstractStrategy):
    def __init__(self, game):
        AbstractStrategy.__init__(self, game)
        self._actions = ['left', 'right', 'turnleft', 'turnright', 'down', 'drop']
        self.field = self._game.me.field

    def choose(self):
        field = self._game.me.field.npField
        field[field == 1] = 0
        width = self._game.me.field.width
        height = self._game.me.field.height
        piece = self._game.piece
        size = piece.size
        piecePos = self._game.piecePosition

        bestHeuristic = -9999999
        bestX = None
        bestY = None
        bestXShape = None
        bestYShape = None
        bestRot = 0
        bestLeft = False

        while piece.turnRight():
            continue
        for r in range(len(piece._rotations)):
            left = piece.turnLeft()
            if not left:
                piece.turnRight(times=len(piece._rotations))
            pieceMatrix = np.zeros(shape=(size, size), dtype=np.int64)
            for coords in piece.positions():
                pieceMatrix[coords[0]][coords[1]] = 2
            pieceMatrix = self.trimPiece(pieceMatrix)

            yShape, xShape = pieceMatrix.shape
            for y in range(0, height - (yShape - 1)):
                for x in range(0, width - (xShape - 1)):
                    if not np.any(field[y:y + yShape, x:x + xShape] + pieceMatrix > 2):
                        possibleState = np.copy(field)
                        possibleState[y:y + yShape, x:x + xShape] += pieceMatrix
                        possibleHeuristic = bestPositionHeuristic(possibleState)
                        if np.isclose(possibleHeuristic, max(possibleHeuristic, bestHeuristic)):
                            bestHeuristic = possibleHeuristic
                            bestXShape, bestYShape = xShape, yShape
                            bestX = x
                            bestY = y
                            bestRot = r + 1
                            bestLeft = left
        if bestLeft:
            direction = 'turnleft'
        else:
            direction = 'turnright'
            bestRot -= len(piece._rotations)

        return self.findPath(bestRot, direction, field, bestX, bestY, piecePos[0], piecePos[1] + 1, bestXShape, bestYShape, width, height)

    def trimPiece(self, piece):
        columns = np.all(piece == 0, axis=0)
        dec = 0
        for i, column in enumerate(columns):
            i -= dec
            if np.any(column):
                piece = np.delete(piece, i, 1)
                dec += 1

        rows = np.all(piece == 0, axis=1)
        dec = 0
        for i, row in enumerate(rows):
            i -= dec
            if np.any(row):
                piece = np.delete(piece, i, 0)
                dec += 1
        return piece

    def findPath(self, rotations, direction, field, bestX, bestY, currX, currY, xShape, yShape, width, height):
        initElement = Element(currX, currY, 0, "", None)
        pathList = [initElement]
        closedList = [initElement]
        goalNode = None
        moves = [direction] * rotations
        while not goalNode and pathList:
            pathList.sort(key=lambda elem: elem.heuristicValue)
            pathQueue = deque(pathList)
            currElement = pathQueue.popleft()
            pathList = list(pathQueue)
            # Look at the tiles offset to one to left, right, top or bottom
            for offsetX, offsetY, direction in [(1, 0, "left"), (-1, 0, "right"), (0, 1, "down")]:
                nextX, nextY = currElement.x + offsetX, currElement.y + offsetY
                nextElement = Element(nextX, nextY, bestPathHeuristic(nextX, nextY, currX, currY), direction, currElement)

                if 0 <= nextX < width - (xShape - 1) and 0 <= nextY < height - (yShape - 1) and \
                        not np.any(field[nextY:nextY + yShape, nextX:nextX + xShape] != 0) and \
                        nextElement not in closedList:

                    closedList.append(nextElement)
                    pathList.insert(0, nextElement)
                    if nextX == bestX and nextY == bestY:
                        goalNode = nextElement
        step = []
        if goalNode:
            while goalNode.parent:
                step.insert(0, goalNode.direction)
                goalNode = goalNode.parent
        step.append("drop")
        moves.extend(step)
        return moves
