import math
import numpy as np

ahMult = -0.510066
clMult = 1.360666
hMult = -0.55663
bMult = -0.184483
mhMult = -0.8
fcMult = -0.2


def bestPathHeuristic(x, y, bestX, bestY):
    # Manhattan Distance
    return abs(x - bestX) + abs(y - bestY)


def bestPositionHeuristic(field):
    height, width = field.shape
    agHeight, maxHeight = agHeightAndMaxHeight(field, width, height)
    return ahMult * agHeight + \
           clMult * completeLines(field, width) + \
           hMult * holes(field, width) + \
           bMult * bumpiness(field, width, height) + \
           mhMult * maxHeight


def holes(field, width):
    columns = np.hsplit(field, width)
    count = 0
    for column in columns:
        block = False
        for row in column:
            if row == 2:
                block = True
            elif row[0] != 2 and block:
                count += 1
    return count


def agHeightAndMaxHeight(field, width, height):
    columns = np.hsplit(field, width)
    agHeight = 0
    maxHeight = 0
    for column in columns:
        currHeight = columnHeight(column, height)
        agHeight += currHeight
        if currHeight > maxHeight:
            maxHeight = currHeight
    return agHeight, maxHeight


def bumpiness(field, width, height):
    columns = np.hsplit(field, width)
    bumpiness = 0
    for i in range(width - 1):
        bumpiness += math.fabs(columnHeight(columns[i], height) - columnHeight(columns[i + 1], height))
    return bumpiness


# def fillCt(field):
#     x, y = np.where(field == 2)
#     return len(x)


def columnHeight(column, height):
    y, x = np.where(column == 2)
    if len(y) > 0:
        return height - y[0]
    return 0


def completeLines(field, width):
    columns = np.hsplit(field, width)
    completeLines = 0
    for column in columns:
        if np.all(column == 2):
            completeLines += 1
    return completeLines
