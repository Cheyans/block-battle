class Element:
    def __init__(self, x, y, heuristicValue, direction, parent):
        self.x = x
        self.y = y
        self.heuristicValue = heuristicValue
        self.direction = direction
        self.parent = parent

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def __str__(self):
        return str(self.x) + ", " + str(self.y) + ", " + str(self.heuristicValue)
