QtWidgets# Python 3.4*
'''
Overly simple python3 starter bot
'''

import sys


def run():
    while not sys.stdin.closed:
        try:
            rawline = sys.stdin.readline()
            # End of file check
            if len(rawline) == 0:
                break
            line = rawline.strip()
            # Empty lines can be ignored
            if len(line) == 0:
                continue
            parts = line.split()
            command = parts[0]
            if command == 'settings':
                pass
            elif command == 'update':
                pass
            elif command == 'action':
                print("test")
                sys.stdout.write('drop\n')
                sys.stdout.flush()
        except EOFError:
            return

if __name__ == "__main__":
    run()
